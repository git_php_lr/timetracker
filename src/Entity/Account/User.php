<?php

 

namespace App\Entity\Account;

use Doctrine\ORM\Mapping as ORM;
use Pd\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User Accounts.
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\Account\UserRepository")
 * @UniqueEntity(fields="email", message="email_already_taken")
 *
 *   
 */
class User extends BaseUser
{
}
