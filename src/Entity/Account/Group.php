<?php

 

namespace App\Entity\Account;

use Doctrine\ORM\Mapping as ORM;
use Pd\UserBundle\Model\Group as BaseGroup;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User Groups.
 *
 * @ORM\Table(name="user_group")
 * @ORM\Entity(repositoryClass="App\Repository\Account\GroupRepository")
 * @UniqueEntity(fields="name", message="group_already_taken")
 *
 *   
 */
class Group extends BaseGroup
{
}
