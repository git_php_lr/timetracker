<?php

 

namespace App\Repository\System;

use App\Entity\System\Config;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class ConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Config::class);
    }

    /**
     * Get by Config Names.
     */
    public function loadByNames(array $names): QueryBuilder
    {
        return $this->createQueryBuilder('c')
            ->where('c.name IN (:names)')
            ->setParameter('names', $names);
    }
}
