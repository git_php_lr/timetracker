<?php

 

namespace App\Service;

/**
 * Tools.
 *
 *   
 */
class Tools
{
    /**
     * Get Project Root Directory.
     */
    public static function rootDir(string $path = ''): string
    {
        return \dirname(__DIR__, 2).'/'.$path;
    }

    /**
     * Get Upload Dir.
     */
    public static function uploadDir(string $file = ''): string
    {
        return self::rootDir($_ENV['UPLOAD_DIR'].$file);
    }
}
