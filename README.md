# Lrcommerce
lrcommerce Commerce is common ecommerce platform with admin and storefront all required features.

## Getting Started

### Prerequisites

List any prerequisites that developers need to install or configure before using your Symfony project. Common prerequisites include:

- PHP (8.2)
- Composer
- Symfony CLI (if applicable)
- Web server (e.g., Apache, Nginx)

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/git_php_lr/lrecommercedemo.git
   
2. Config DB in .env file:
   ```bash
   DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name"   
   
3. Install project:
   ```bash
   composer install
   php bin/console doctrine:database:create
   php bin/console doctrine:migrations:migrate
   
4. Start server:
   ```bash
    symfony server:start
